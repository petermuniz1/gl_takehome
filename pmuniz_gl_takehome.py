import yaml

gl_dept_groups = ["eng-dev-manage-access","eng-dev-manage-compliance","eng-dev-manage-import","eng-dev-manage-analytics","eng-dev-plan","eng-dev-plan-project-mgmt","eng-dev-plan-portfolio-mgmt","eng-dev-plan-certify","eng-dev-create","eng-dev-create-source-code","eng-dev-create-editor","eng-dev-create-gitaly","eng-dev-create-gitter","eng-dev-create-ecosystem","eng-dev-verify","eng-dev-verify-ci","eng-dev-verify-runner","eng-dev-verify-testing","eng-dev-package","eng-dev-package-package","eng-dev-release","eng-dev-release-progressive-delivery","eng-dev-release-release-mgmt","eng-dev-configure","eng-dev-configure-configure","eng-dev-monitor","eng-dev-monitor-apm","eng-dev-monitor-health","eng-dev-secure","eng-dev-secure-static-analysis","eng-dev-secure-dynamic-analysis","eng-dev-secure-composition-analysis","eng-dev-secure-fuzz-testing","eng-dev-secure-threat-insights","eng-dev-secure-research","eng-dev-protect","eng-dev-protect-container-security","eng-dev-growth","eng-dev-growth-acquisition","eng-dev-growth-conversion","eng-dev-growth-expansion","eng-dev-growth-retention","eng-dev-growth-fulfillment","eng-dev-growth-telemetry","eng-dev-enablement","eng-dev-enablement-distribution","eng-dev-enablement-geo","eng-dev-enablement-memory","eng-dev-enablement-search","eng-dev-enablement-database","eng-infra","eng-infra-analytics","eng-infra-scalability","eng-infra-deliverability","eng-infra-reliability","eng-quality","eng-quality-ops-ci-cd","eng-quality-secure-enablement","eng-quality-productivity","eng-quality-growth-protect","eng-security","eng-security-ops-red","eng-security-ops-incident-response","eng-security-ops-trust-safety","eng-security-risk-compliance","eng-security-eng-app-sec","eng-security-eng-automation","eng-security-eng-research","eng-support","eng-support-saas","eng-support-self-managed","eng-support-americas","eng-support-emea","eng-support-apac","eng-ux","eng-ux-technical-writing","eng-ux-research"]

gl_roles = ["pm", "pmm", "cm", "backend_engineering_manager", "frontend_engineering_manager", "support", "sets", "pdm", "ux", "uxr", "tech_writer", "tw_backup", "appsec_engineer"]

gl_stages = ["manage", "plan", "create", "verify", "package", "secure", "release", "configure", "monitor", "protect", "fulfillment", "growth", "enablement", "learn", "mobile", "deploy"]

gl_team = []

manager_roles = ["backend_engineering_manager","frontend_engineering_manager"]

with open(r'./stages.yml') as file:
    stages_yml = yaml.load(file, Loader=yaml.FullLoader)

with open(r'./team.yml') as file:
    team_yml = yaml.load(file, Loader=yaml.FullLoader)

for group in gl_dept_groups:
    gl_team.append("\ngroup name: " + group + "\n")
    for role in gl_roles:
        try:
            path1 = group.split("-")[-2]
            path2 = group.split("-")[-1]
            #print(path1, path2)
            for key, value in stages_yml["stages"][path1]["groups"][path2].items():
                if key == role:
                    #print("group name: ", group, "\n")
                    #print("    ", key, ":", value, "\n")
                    for x in team_yml:
                        if value in x["name"]:
                            handle = x["gitlab"]
                    if role in manager_roles:

                        gl_team.append("    *Group Manager*    " + key + ": " + value + "\n        member_handle: " + handle)
                    else:
                        gl_team.append("    " + key + ": " + value + "\n        member_handle: " + handle)
                    handle = ""
        except:
            pass

for item in gl_team:
    print(item)